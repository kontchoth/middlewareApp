var VendorController = require('./controllers/VendorController');
var AuthenticationController = require('./controllers/AuthenticationController');
var UserController = require('./controllers/UserController');
var StoreController = require('./controllers/StoreController');
var ProductController = require('./controllers/ProductController');
var ProductionController = require('./controllers/ProductionController');

module.exports = {
    '/vendors': {
        restfull: true,
        controller: VendorController
    },
    'auth': {
        restfull: false,
        controller: AuthenticationController
    },
    'user': {
        restfull: false,
        controller: UserController
    },
    'stores': {
        restfull: false,
        controller: StoreController,
    },
    'products': {
        restfull: false,
        controller: ProductController
    },
    'productions': {
        restfull: false,
        controller: ProductionController
    }
};