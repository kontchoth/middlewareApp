var _ = require('lodash');
var passport = require('passport');

module.exports = {
    getAllData: function (req, res, db) {
        db.find({}, function (error, data) {
            if (error) {
                throw error;
            }
            res.send({ success: true, data: data, count: data.length });
        });
    },
    createObject: function (req, res, db) {
        if (!req.body.data) {
            res.status(400).send({ success: false, error: 'Missing Information.' });
        } else {
            var newObj = new db(req.body.data);

            // attempt to save it
            newObj.save(function (err) {
                if (err) {
                    return res.status(300).send({ success: false, errors: err });
                }
                res.json({ success: true });
            })
        }
    },
    updateObject: function (req, res, db) {
        if (!req.body.data) {
            res.status(400).send({ success: false, error: 'Data missing.' });
        } else {
            var data = req.body.data;
            data.updated = new Date();
            db.update({ _id: data._id }, {
                $set: data
            }, function (err, obj) {
                if (err) {
                    return res.status(400).send({ success: false, error: 'Could not update data information' });
                }
                res.json({ success: true, message: 'Data has been updated' });
            });
        }
    },
    querySingleObject: function (req, res, db) {
        if (!req.params.objID) {
            return res.send({ success: false, error: 'Object ID is missing' });
        } else {
            db.findOne({ objID: req.params.objID.toString() }, function (error, data) {
                if (error) {
                    return res.status(404).send({ success: false, error: 'No object found with that ID.' });
                }
                if (data === null) {
                    return res.status(404).send({ success: false, error: 'Invalid data ID.' })
                }

                res.json({ success: true, data: data });
            });
        }
    },
    deleteSingleObject: function (req, res, db) {
        if (!req.params.objID) {
            return res.status(400).send({ success: false, error: "Missing Information" });
        } else {
            db.remove({ objID: req.params.objID.toString() }, function (err, data) {
                if (err) {
                    return res.status(400).send({ success: false, error: err });
                }
                res.json({ success: true, data: 'Object Deleted.' });
            });
        }
    },
    registerAPI: function (app, apis) {
        _.each(apis, function (api) {
            switch (api.method) {
                case 'POST':
                    if (api.reqAuth) {
                        app.post(api.url, passport.authenticate('jwt', { session: false }), api.handler);
                    } else {
                        app.post(api.url, api.handler);
                    }
                    break;
                case 'GET':
                    if (api.reqAuth) {
                        app.get(api.url, passport.authenticate('jwt', { session: false }), api.handler);
                    } else {
                        app.get(api.url, api.handler);
                    }
                    break;
                case 'PUT':
                    if (api.reqAuth) {
                        app.put(api.url, passport.authenticate('jwt', { session: false }), api.handler);
                    } else {
                        app.put(api.url, api.handler);
                    }
                    break;
                case 'DELETE':
                    if (api.reqAuth) {
                        app.delete(api.url, passport.authenticate('jwt', { session: false }), api.handler);
                    } else {
                        app.delete(api.url, api.handler);
                    }
                    break;
                default:
                    break;
            }
        });
    }
};