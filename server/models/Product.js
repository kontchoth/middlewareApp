var mongoose = require('mongoose');

// Ingredient details
var IngredientSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    unit: {
        type: String,
        required: true,
        trim: true,
        enum: ['ounce(s)', 'gram(s)', 'slide(s)'],
        default: 'ounce(s)'
    },
    quantity: {
        type: Number,
        required: true,
        default: 0.0
    }
});

// Product Schema
var ProductSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: true,
        unique: true
    },
    upc: {
        type: Number,
        required: true
    },
    objID: {
        type: String,
        required: true,
        trim: true
    },
    description: {
        type: String,
        default: ''
    },
    vin: {
        type: String,
        required: true,
        trim: true
    },
    ingredients: {
        type: [IngredientSchema],
        required: false,
        default: []
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Product', ProductSchema);