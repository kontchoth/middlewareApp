var mongoose = require('mongoose');

// sale Products
var saleProductSchema = new mongoose.Schema({
    product: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Product',
        required: true
    },
    qtySold: {
        type: Number,
        required: true,
        default: 0
    },
    unitCost: {
        type: Number,
        required: true,
        default: 0
    },
    totalCost: {
        type: Number,
        required: true,
        default: 0
    }
});

// production schema
var ProductionSchema = new mongoose.Schema({
    date: {
        type: Date,
        required: true,
    },
    storeID: {
        type: Number,
        required: true
    },
    items: {
        type: [saleProductSchema],
        required: true,
        default: []
    }
});

module.exports = mongoose.model('Production', ProductionSchema);