// export all the models
module.exports = {
    vendor: require('./Vendor.js'),
    user: require('./User.js'),
    store: require('./Store.js'),
    product: require('./Product.js'),
    production: require('./Production.js')
};