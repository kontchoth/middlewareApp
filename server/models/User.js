var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');
var config = require('../config/main');

var AddressSchema = new Schema({
    streetname: {
        type: String,
        required: false,
        trim: true
    },
    city: {
        type: String,
        required: false,
        trim: true
    },
    state: {
        type: String,
        required: false,
    },
    zipcode: {
        type: String,
        required: false,
        trim: true
    }
});

var NameSchema = new Schema({
    firstname: {
        type: String,
        trim: true,
        required: false,
        default: ''
    },
    middlename: {
        type: String,
        trim: true,
        required: false,
        default: ''
    },
    lastname: {
        type: String,
        trim: true,
        required: false,
        default: ''
    }
});

var UserSchema = new Schema({
    name: {
        type: NameSchema,
        required: false,
        default: null
    },
    role: {
        type: [String],
        required: false,
        enum: ['Employee', 'Manager', 'Admin'],
        defaulf: ['Employee']
    },
    address: {
        type: AddressSchema,
        required: false,
        default: null
    },
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
        trim: true,
        select: false
    }
});

// Save the user password hashed (plain text password storage is not good)
UserSchema.pre('save', function(next) {
    var user = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function(err, salt) {
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.password, salt, function(err, hash) {
                if(err) {
                    return next(err);
                }
                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});

// Create a method to compare password input to password saved in the database
UserSchema.methods.comparePassword = function(pw, callback) {
    bcrypt.compare(pw, this.password, function(err, isMatch) {
        if (err) {
            return callback(err);
        }
        callback(null, isMatch);
    });
};

// export the model schema
module.exports = mongoose.model('User', UserSchema);