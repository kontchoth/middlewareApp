var mongoose = require('mongoose');

// address Schema
var AddressSchema = new mongoose.Schema({
    streetname: {
        type: String,
        required: false,
        trim: true,
        default: ''
    },
    city: {
        type: String,
        required: false,
        trim: true,
        default: ''
    },
    state: {
        type: String,
        required: false,
        trim: true,
        default: ''
    },
    zipcode: {
        type: String,
        required: false,
        trim: true,
        default: ''
    }
});

// contact Schema
var ContactSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: false,
        trim: true,
        default: ''
    },
    phone: {
        type: String,
        required: false,
        trim: true,
        default: ''
    },
    fax: {
        type: String,
        required: false,
        trim: true,
        default: ''
    }
});

// create the Vendor Schema
var VendorSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    address: {
        type: AddressSchema,
        required: false
    },
    contact: {
        type: ContactSchema,
        required: false,
        default: null
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    }
});

// export the model schema
module.exports = VendorSchema;