'use strict';


var serverMessages = [
    {name: 'client:authentication', handler: authenticate, roles: ['all', 'public']},
    {name: 'client:init', handler: initialization, roles: ['all']}
];

function authenticate(data) {
    console.log('Calling authentication', data);
}

function initialization(data) {
    console.log('Initialization', data);
}

module.exports = {
    serverMessage: serverMessages
};