'use strict';
var utils  = require('./utils');
var Client = require('node-rest-client').Client;

var client = new Client({
    connection: {
        rejectUnauthorized: false
    }
});


// where is our identity management services
var identServer = null;
var identUrl = null;
var identTimeout = null;
loadService();


// expose our public interfaces for all to consume
//
module.exports = { 
    login:   login,
    logout:  logout,
    renew:   renew,
    details: details,
    loadService: loadService
};


function loadService() {

    var config = require('../config.json');
    identServer = config["vibe-identity"];
    identUrl = identServer.protocol + '://' + identServer.hostname + ':' + identServer.port + identServer.restURL;
    identTimeout = (config["vibe-identity"].timeout) ? config["vibe-identity"].timeout: 10000;

    // define the API REST methods
    //
    client.registerMethod( "identLogin",   identUrl + '/login',   "POST");
    client.registerMethod( "identLogout",  identUrl + '/logout',  "PUT");
    client.registerMethod( "identRenewal", identUrl + '/renewal', "PUT");
    client.registerMethod( "identDetails", identUrl + '/details/${token}', "GET");
};

//
// Login using the given credentials
// credentials {
//    username: name of user to login with,
//    password: password to use
// }
//
// Returns standard response of success/error
//
// success returns:
// {
//  "token": "b146753616764aa183ae8c783fa6b992",
//  "details": {
//    "ttl": 1800,
//    "username": "ntcadmin",
//    "application": "xSight",
//    "roles": [
//      "Admin"
//    ]
//  }
// }
//
function login( credentials, callback ) {    
    var operation = "vibeIdentity:login";
    
    if(credentials && credentials.username && credentials.password) {

        // extra stuff added to credentials
        credentials["application"] = "xSIGHT";
        
        var args = {
            data: credentials
        };
        utils.execMethod( operation, client.methods.identLogin, args, identTimeout, callback );
    }
    else {
        if(callback) callback(utils.makeErrorResponse(operation, 401, "Username or password is missing"));
    }
};

//
// logout the given login token
//   "token" : "3d5be968b79f4cdebf291974ced77cef"
//
function logout( token, callback ) {    
    var operation = "vibeIdentity:logout";
    
    if(token) {
        var args = {
            data: { token: token }
        };
        utils.execMethod( operation, client.methods.identLogout, args, identTimeout, callback );
    }
    else {
        if(callback) callback(utils.makeErrorResponse(operation, 401, "Login token is missing"));
    }
};


//
// renew the given login token
// {
//   "token" : "3d5be968b79f4cdebf291974ced77cef"
// }
//
function renew( token, callback ) {    
    var operation = "vibeIdentity:renew";
    
    if(token) {
        var args = {
            data: { token: token }
        };
        utils.execMethod( operation, client.methods.identRenewal, args, identTimeout, callback );
    }
    else {
        if(callback) callback(utils.makeErrorResponse(operation, 401, "Login token is missing"));
    }
};


//
// return details for the given user token
// token is a string.
//
// success returns:
// {
//  "token": "b146753616764aa183ae8c783fa6b992",
//  "details": {
//    "ttl": 1800,
//    "username": "ntcadmin",
//    "application": "xSight",
//    "roles": [
//      "Admin"
//    ]
//  }
// }
//
function details( token, callback ) {    
    var operation = "vibeIdentity:details";
    
    if(token && token !== 'empty') {
        var args = {
            path: { "token": token },
        };
        utils.execMethod( operation, client.methods.identDetails, args, identTimeout, callback );
    }
    else {
        if(callback) callback(utils.makeErrorResponse(operation, 401, "Login token is missing"));
    }
};
