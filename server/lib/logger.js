var fs = require('fs');
var path = require('path');
var winston = require('winston');
winston.emitErrs = true;

// make sure the log file exists
var logpath = path.join(__dirname + '/../log/app.log');
fs.closeSync(fs.openSync(logpath, 'a'));

var accesspath = path.join(__dirname, '/../log/access.log');
fs.closeSync(fs.openSync(accesspath, 'a'));

winston.loggers.add('mainLog', {
    console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true
    },
    file: {
        level: 'debug',
        filename: logpath,
        handleExceptions: true,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false,
        tailable: true
    }
});

winston.loggers.add('accessLog', {
    console: {
        silent: true
    },
    file: {
        level: 'silly',
        filename: accesspath,
        handleExceptions: true,
        json: false,
        maxsize: 5242880, 
        maxFiles: 5,
        colorize: false,
        tailable: true
    }
});

var mainLogger = winston.loggers.get('mainLog');
var accessLogger = winston.loggers.get('accessLog');

module.exports = mainLogger;
module.exports.stream = {
    write: function(message, encoding) {
        accessLogger.silly(message);
    }
};