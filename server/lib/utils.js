'use strict';
var logger = require('./logger');
var serverConfig = require('../config.json');

// expose interface
module.exports = {
    makeResponse: makeResponse,
    makeErrorResponse: makeErrorResponse,
    errorMessage: errorMessage,
    handleResponse: handleResponse,
    handleRequestTimeout: handleRequestTimeout,
    handleResponseTimeout: handleResponseTimeout,
    execMethod: execMethod,
    requireReload: requireReload,
    cleanupSettingFile: cleanupSettingFile
};

function cleanupSettingFile(id) {
    var fs = require('fs');
    var prefix = '../resource/';
    var filePath = prefix + id.substring(2) + '/setting.middleware';

    fs.access(filePath, fs.F_OK, function(err) {
        if(!err){
            fs.unlink(filePath, function(err) {
                if(err) {
                    logger.info(err);
                } else {
                    var folderPath = filePath.substring(0, filePath.length - 12);
                    fs.rmdir(folderPath, function(err) {
                        if(err) {
                            logger.info(err);
                        }
                    });
                }
            });
        } else {
            // file is not accessible do nothing
        }
    });
}

function makeResponse(operation, data) {
    return {
        op: operation,
        status: "success",
        data: (data) ? data : {}
    };
}

function makeErrorResponse(operation, code, message) {
    return {
        op: operation,
        status: "error",
        data: errorMessage(code, message)
    };
}

function errorMessage(code, message) {
    var obj = { code: code, message: message };
    return obj;
}

function execMethod(operation, method, args, timeout, callback, dataToSendAck) {
    if (args && !args.headers) {
        args.headers = {};
    }

    if (!args.headers['Content-Type']) args.headers['Content-Type'] = "application/json";
    if (!args.headers['Connection']) args.headers['Connection'] = "keep-alive";
    if (!args.headers['Accept']) args.headers['Accept'] = "*/*";
    if (!args.headers['Accept-Encoding']) args.headers['Accept-Encoding'] = "gzip, deflate";
    if (!args.headers['Accept-Language']) args.headers['Accept-Language'] = "en-US,en;q=0.8";

    if (args && !args.requestConfig) {
        args.requestConfig = {};
    }
    if (!args.requestConfig.timeout) args.requestConfig.timeout = timeout;

    if (args && !args.responseConfig) {
        args.responseConfig = {};
    }
    if (!args.responseConfig.timeout) args.responseConfig.timeout = timeout;

    method(args, function (data, response) {
        handleResponse(operation, data, response, callback, dataToSendAck);
    }).on('error', function (err) {
        handleError(operation, err, callback);
    }).on('requestTimeout', function (req) {
        handleRequestTimeout(req, operation, callback);
    }).on('responseTimeout', function (res) {
        handleResponseTimeout(operation, callback);
    });
}

function requireReload(path) {
    delete require.cache[require.resolve(path)];
    return require(path);
}

function handleResponse(operation, data, response, callback, dataToSendAck) {
    if (callback) {
        var res = makeResponse(operation);
        if (response && response.statusCode < 300) {
            if (dataToSendAck) {
                res.data = dataToSendAck;
            } else if (data instanceof Uint8Array) {
                try {
                    if (response.headers['content-type'] === 'application/json') {
                        res.data = JSON.parse(String.fromCharCode.apply(null, data));
                    } else if (response.headers['content-type'] === 'text/plain') {
                        res.data = { message: String.fromCharCode.apply(null, data) };
                    } else {
                        res.data = { message: response.statusMessage };
                    }
                } catch (err) {
                    res.status = 'error';
                    res.data = errorMessage(500, operation + " does not understand data returned from REST API");
                }
            } else if (data instanceof Object) {
                res.data = data;
            } else {
                res.data = { response: data };
            }
        } else {
            // treat as error
            res.status = 'error';

            var message = response.statusMessage;
            if (data && data.error && data.error.reason) {
                message = data.error.reason;
            }

            if (response.headers['content-type'] === 'text/plain') {
                if (data instanceof Uint8Array) {
                    message = String.fromCharCode.apply(null, data);
                } else if (data instanceof String) {
                    message = data;
                }
            } else if (response.headers['content-type'] === 'application/json') {
                if (data.errorMessage) message = data.errorMessage;
            }
            res.data = errorMessage(response.statusCode, message);
        }
        callback(res);
    }
}

function handleRequestTimeout(req, operation, callback) {
    logger.error('request has expired');
    req.abort();
    if (callback) {
        callback(makeErrorResponse(operation, 408, 'Request Timeout'));
    }
};

function handleResponseTimeout(operation, callback) {
    logger.error('Response has expired');
    if (callback) {
        callback(makeErrorResponse(operation, 500, 'Response Time out'));
    }
}