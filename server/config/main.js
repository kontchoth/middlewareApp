module.exports = {
    secret: 'middlewareapplicationsecretkey',
    database: 'mongodb://localhost/middleware',
    expireTime: 10000,
    refreshTime: 10000
};