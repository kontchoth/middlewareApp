var Product = require('../models/Product');
var genericAPICalls = require('../generic/GenericConstructor');
var path = require('path');
var formidable = require('formidable');
var fs = require('fs');

function getAll(req, res) {
    return genericAPICalls.getAllData(req, res, Product);
}

function create(req, res) {
    return genericAPICalls.createObject(req, res, Product);
}

function update(req, res) {
    return genericAPICalls.updateObject(req, res, Product);
}

function querySingle(req, res) {
    return genericAPICalls.querySingleObject(req, res, Product);
}

function deleteSingle(req, res) {
    return genericAPICalls.deleteSingleObject(req, res, Product);
}


var apis = [
    {
        url: '/products',
        method: 'GET',
        handler: getAll,
        roles: ['All'],
        reqAuth: true
    }, {
        url: '/products',
        method: 'POST',
        handler: create,
        roles: ['All'],
        reqAuth: true
    }, {
        url: '/products',
        method: 'PUT',
        handler: update,
        roles: ['All'],
        reqAuth: true
    }, {
        url: '/products/:objID',
        method: 'GET',
        handler: querySingle,
        roles: ['All'],
        reqAuth: true
    },{
        url: '/products/:objID',
        method: 'DELETE',
        handler: deleteSingle,
        roles: ['All'],
        reqAuth: true
    }
];

module.exports = function(app) {
    genericAPICalls.registerAPI(app, apis);
};