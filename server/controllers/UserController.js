var express = require('express');
var apiRoutes = express.Router();
var User = require('../models/User');
var jwt = require('jsonwebtoken');
var restful = require('node-restful');
var config = require('../config/main');
var passport = require('passport');
var bcrypt = require('bcrypt');

function getUserInformation(req, res) {
    if(!req.user._id) {
        res.json({success: false, message: 'Unauthenticated!!!'});
    } else {
        User.findOne({
            _id: req.user._id
        }, function(err, user) {
            if (err) {
                throw err;
            }

            if (!user) {
                res.send({success: false, message: 'Unable to find user detail.'});
            } else {
                res.send({sucess: true, data: user});
            }
        });
    }
}

function updateUserDetail(req, res) {
    if (!req.body.user) {
        res.send({success: false, message: 'User nnot present.'});
    } else {
        var req_user = req.body.user;
        
        // hash password if there is any
        if (req_user && req_user.password) {
            bcrypt.genSalt(10, function(err, salt) {
                if (!err) {
                    bcrypt.hash(req_user.password, salt, function(err, hash) {
                        if(!err) {
                            req_user.password = hash;
                            User.update({_id: req.body.user._id}, {
                                $set: req.body.user
                            }, function(err, user) {
                                if (err) {
                                    console.log('Could not update');
                                    console.log(err);
                                    res.send({success: false, error: err});
                                }
                                console.log('updated');
                                console.log(user);
                                res.json({success: true, message: 'User details updated.', data: user});
                            });
                        }
                    });
                }
            });
        }
    }
}

module.exports = function(app){
    app.get('/api/user-info', passport.authenticate('jwt', { session: false }), getUserInformation);
    app.post('/api/user-info', passport.authenticate('jwt', { session: false }), updateUserDetail);
};