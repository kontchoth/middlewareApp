var Store = require('../models/Store');
var _ = require('lodash');
var passport = require('passport');

function getAllStore(req, res) {
    Store.find({}, function(error, stores) {
        if (error) {
            throw error;
        }
        res.send({success: true, data: {count: stores.length, stores: stores}});
    });
}

function createStore(req, res) {
    if (!req.body.store) {
        res.json({success: false, message: 'Missing Information'});
    } else {
        var newStore = new Store(req.body.store);
        // attemp to save it

        newStore.save(function(err) {
            if (err) {
                return res.send({success: false, error: err});
            }
            res.json({success: true, message: 'Store created.'});
        })
    }
}

function updateStore(req, res) {
    if (!req.body.store) {
        res.status(400).send({success: false, error: 'Store missing.'});
    } else {
        var store = req.body.store;
        store.updated = new Date();
        Store.update({_id: req.body.store._id}, {
            $set: store
        }, function(err, store) {
            if (err) {
                return res.status(400).send({success: false, error: 'Could not update Store information'});
            }
            res.json({success: true, message: 'Store has been updated'});
        });
    }
}

function getSingleStore(req, res) {
    if (!req.params.storeID) {
        return res.send({success: false, error: 'Store ID is missing'});
    } else {
        Store.findOne({storeID: req.params.storeID}, function(error, store) {
            if (error) {
                return res.status(404).send({success: false, error: 'No store found with that ID.'});
            }
            if (store === null) {
                return res.status(404).send({success: false, error: 'Invalid store ID.'})
            }

            res.json({success: true, data: store});
        });
    }
}


var apis = [
    {
        url: '/stores',
        method: 'GET',
        handler: getAllStore,
        roles: ['All'],
        reqAuth: true
    }, {
        url: '/stores',
        method: 'POST',
        handler: createStore,
        roles: ['All'],
        reqAuth: true
    }, {
        url: '/stores',
        method: 'PUT',
        handler: updateStore,
        roles: ['All'],
        reqAuth: true
    }, {
        url: '/stores/:storeID',
        method: 'GET',
        handler: getSingleStore,
        roles: ['All'],
        reqAuth: true
    }
];

module.exports = function(app) {
    _.each(apis, function(api) {
        switch(api.method) {
            case 'POST':
                if (api.reqAuth) {
                    app.post(api.url, passport.authenticate('jwt', { session: false }), api.handler);
                } else {
                    app.post(api.url, api.handler);
                }
                break;
            case 'GET':
                if (api.reqAuth) {
                    app.get(api.url, passport.authenticate('jwt', { session: false }), api.handler);
                } else {
                    api.get(api.url, api.handler);
                }
                break;
            case 'PUT':
                if (api.reqAuth) {
                    app.put(api.url, passport.authenticate('jwt', { session: false }), api.handler);
                } else {
                    api.put(api.url, api.handler);
                }
                break;
            default:
                break;
        }
    });
};