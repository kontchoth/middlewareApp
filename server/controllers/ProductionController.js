var DBTable = require('../models/Production');
var genericAPICalls = require('../generic/GenericConstructor');

function getAll(req, res) {
    return genericAPICalls.getAllData(req, res, DBTable);
}

function create(req, res) {
    return genericAPICalls.createObject(req, res, DBTable);
}

function update(req, res) {
    return genericAPICalls.updateObject(req, res, DBTable);
}

function querySingle(req, res) {
    return genericAPICalls.querySingleObject(req, res, DBTable);
}

function deleteSingle(req, res) {
    return genericAPICalls.deleteSingleObject(req, res, DBTable);
}

function uploadProductionFile(req, res) {
    // Create an incoming form object
    var form = new formidable.IncomingForm();

    // Specify that we want to allow the user to upload multiple file(s)
    // in a single request
    form.multiples = true;

    // store all uploads file in the /upload directory
    form.uploadDir = path.join(__dirname, '../uploads');

    // everytime a file has been uploaded successfully
    // rename it to it's original name'
    form.on('file', function(field, file) {
        var date = new Date().toString();
        fs.rename(file.path, path.join(form.uploadDir, date + '_' + file.name));
    });

    // log any errors that occur
    form.on('error', function(err) {
        console.log('An error has occured: ', err);
    });

    form.on('end', function() {
        res.end('success');
    });

    form.parse(req);
}

var apis = [
    {
        url: '/productions',
        method: 'GET',
        handler: getAll,
        roles: ['All'],
        reqAuth: true
    }, {
        url: '/productions',
        method: 'POST',
        handler: create,
        roles: ['All'],
        reqAuth: true
    }, {
        url: '/productions',
        method: 'PUT',
        handler: update,
        roles: ['All'],
        reqAuth: true
    }, {
        url: '/productions/:objID',
        method: 'GET',
        handler: querySingle,
        roles: ['All'],
        reqAuth: true
    },{
        url: '/productions/:objID',
        method: 'DELETE',
        handler: deleteSingle,
        roles: ['All'],
        reqAuth: true
    },{
        url: '/productions/uploads',
        method: 'POST',
        handler: uploadProductionFile,
        roles: ['All'],
        reqAuth: true
    }
];



module.exports = function(app) {
    genericAPICalls.registerAPI(app, apis);
};