var express = require('express');
var apiRoutes = express.Router();
var User = require('../models/User');
var jwt = require('jsonwebtoken');
var restful = require('node-restful');
var config = require('../config/main');
var passport = require('passport');
var _ = require('lodash');

// Register new users


// function definition
function userRegistration(req, res) {
    if (!req.body.email || !req.body.password) {
        res.json({success: false, message: 'Please enter email and password'});
    } else {
        var newUser = new User({
            email: req.body.email,
            password: req.body.password,
            role: req.body.roles || ['Employee']
        });

        // attempt to save the user
        newUser.save(function(err) {
            if (err) {
                // console.log(err);
                return res.json({success: false, message: 'That email address already exists.'});
            }
            res.json({success: true, message: 'User created'});
        });
    }
}

function authenticate(req, res) {
    if (!req.body.email || !req.body.password) {
        res.json({success: false, message: 'Email and/or password missing'});
    } else {
        User.findOne({email: req.body.email}).select('+password').exec(function(err, user) {
            if (err) {
                throw err;
            }
            // console.log(user);

            if (!user){
                res.send({success: false, message: 'Authentication failed. User not found.'});
            } else {
                // check if password matches
                user.comparePassword(req.body.password, function(err, isMatch) {
                    if (isMatch && !err) {
                        // create token if the password matched and no error was thrown
                        var token = jwt.sign(user, config.secret, {
                            expiresIn: config.expireTime
                        });
                        res.json({success: true, token: 'JWT ' + token});
                    } else {
                        res.send({success: false, message: 'Authentication failed. User and/or password invalid.'});
                    }
                });
            }
        });
    }
}

function getUserInformation(req, res) {
    if(!req.user._id) {
        res.json({success: false, message: 'Unauthenticated!!!'});
    } else {
        User.findOne({
            _id: req.user._id
        }, function(err, user) {
            if (err) {
                throw err;
            }

            if (!user) {
                res.send({success: false, message: 'Unable to find user detail.'});
            } else {
                res.send({sucess: true, data: user});
            }
        });
    }
}

function updateUserDetail(req, res) {
    if (!req.body.user) {
        res.send({success: false, message: 'User not present.'});
    } else {
        User.findOne({_id: req.body.user._id})
            .select('+password')
            .populate('name')
            .exec(function(err, user){
                console.log('user', user);
                if (err) {
                    throw err;
                }
                if (!user) {
                    res.send({success: false, message: 'Could not find user.'});
                } else {
                    res.json({success: true, data: user});
                }
            });
    }
}

function validateRole(req, res, api, next) {
    if (!req.body.user) {
        res.send({sucess: false, error: 'Unauthorized action.'});
    } else {
        var userRoles = req.user.role;
        for (var i = 0; i < api.roles.length; i++) {
            var index = _.indexOf(userRoles, api.user.role[i]);
            if (index < 0) {
                return next({status: false, message: 'You do not have the correct right.'});
            }
        }
        return next();
    }
}

// list all api available here
var apiRoute = [{
        url: '/register',
        handler: userRegistration,
        authRequired: false,
        roles: ['All'],
        method: 'POST'
    }, {
        url: '/authenticate',
        handler: authenticate,
        authRequired: false,
        method: 'POST',
        roles: ['All']
    }, {
        url: '/api/user-info',
        handler: getUserInformation,
        authRequired: true,
        method: 'POST',
        roles: ['All']
    }
];

module.exports = function(app){
    _.each(apiRoute, function(api) {
        var params = null;
        switch(api.method) {
            case 'POST':
                if(api.authRequired){
                    app.post(api.url, passport.authenticate('jwt', { session: false }), api.handler);
                } else {
                    app.post(api.url, api.handler);
                }
                break;
            case 'GET':
                if(api.authRequired){
                    app.get(api.url, passport.authenticate('jwt', { session: false }), api.handler);
                } else {
                    app.get(api.url, api.handler);
                }
                break;
                    
        }
    });
    // app.post('/register', userRegistration);
    // app.post('/authenticate', authenticate);
    // app.get('/api/user-info', passport.authenticate('jwt', { session: false }), getUserInformation);
};