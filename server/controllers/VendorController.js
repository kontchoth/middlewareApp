// Server Controller used to manage the Vendor DB
// Provide all the CRUD needed for the Vendor Manipulation
var restful = require('node-restful');

module.exports = function(app, route) {

    // setup the controller for REST 
    var rest = restful.model(
        'vendor',
        app.models.vendor
    ).methods(['get', 'put', 'post', 'delete']);

    // Register this endpont with the application
    rest.register(app, route);

    // return middleware.
    return function(req, res, next) {
        next();
    };
};