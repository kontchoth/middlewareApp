var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var _ = require('lodash');
var config = require('./config/main');
var passport = require('passport');
var http = require('http');
var UIComm = require('./lib/ui-comm.js');
var logger = require('./lib/logger');
var utils = require('./lib/utils');

// Create the application
var app = express();
var server = http.createServer(app);
var io = require('socket.io')(server);

// Add middleware necessary for the REST API's
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(methodOverride('X-HTTP-Method-Override'));

// init the passpoer use
app.use(passport.initialize());

// bring in the defined Passport Strategy
require('./config/passport')(passport);


// CORS Support
app.use(function(req, res, next) {
    res.header('Access-Control-All-Origin', '*'); // Make the server open to everybody
    res.header('Access-Control-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});


// app.use('/hello', function(req, res, next) {
//     res.send('Hello Hermann');
//     next();
// });

// connect to MongoDB
mongoose.connect(config.database);
mongoose.connection.once('open', function() {
    // Load all the models. Make them visible accross all the application
    app.models = require('./models/index');

    // Load the route file
    var routes = require('./routes');
    _.each(routes, function(routeConfig, route) {
        if (routeConfig.restfull) {
            console.log('route', route, 'uses restfull');
            app.use(route, routeConfig.controller(app, route));
        } else {
            console.log('route', route, 'does not use restfull');
            routeConfig.controller(app);
        }
        
    });
    
    io.on('connection', function(socket) {
        var user = {
            socket: socket,
            name: socket.handshake.query.username || 'default',
            authenticated: false,
            authTimer: null,
            publicAccess: false
        };

        socket.on('disconnect', function(msg) {
            logger.info('Client ' + user.name + ':' + user.socket.id + ' has disconnected');

            // put any special cleanup code here
            utils.cleanupSettingFile(user.socket.id);
        });


        _.each(UIComm.serverMessage, function(message) {
            console.log('Adding receiver for', message.name);
            socket.on(message.name, function(data) {
                logger.info('ui-client sent ' + message.name);
                console.log(data);
                message.handler(data);
            });
        });

        socket.emit('init', function(data) {
            console.log(data);
        });
    });
    
    console.log('Listening to port 3000 ...');
    server.listen(3000);
});