'use strict';

describe('Controller: VendorsCtrl', function () {

  // load the controller's module
  beforeEach(module('clientApp'));

  var VendorsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    VendorsCtrl = $controller('VendorsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(VendorsCtrl.awesomeThings.length).toBe(3);
  });
});
