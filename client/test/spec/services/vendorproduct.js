'use strict';

describe('Service: VendorProduct', function () {

  // load the service's module
  beforeEach(module('clientApp'));

  // instantiate service
  var VendorProduct;
  beforeEach(inject(function (_VendorProduct_) {
    VendorProduct = _VendorProduct_;
  }));

  it('should do something', function () {
    expect(!!VendorProduct).toBe(true);
  });

});
