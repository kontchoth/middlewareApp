'use strict';

/**
 * @ngdoc service
 * @name clientApp.VendorProduct
 * @description
 * # VendorProduct
 * Factory in the clientApp.
 */
angular.module('clientApp')
    .factory('VendorProduct', ['$q', '$http', 'Authentication', function($q, $http, Authentication) {
        var factory = {};
        var baseURI = 'http://middlewareserver.hermannit.com/';
        var vendorProductApi = {
            getVendorProductList: {
                url: baseURI + 'api/vendors/',
                method: {
                    query: 'GET',
                    update: 'PUT'
                },
                roles: ['all']
            },
            addProduct: {
                url: baseURI + 'api/vendors/',
                method: 'POST',
                roles: ['all']
            },
            getVendor: {
                url: baseURI + 'api/vendors/',
                method: 'POST',
                roles: ['all']
            },
            updateDeleteProduct: {
                url: baseURI,
                method: {
                    query: 'GET',
                    update: 'PUT',
                    delete: 'DELETE'
                },
                roles: ['all']
            }
        };

        factory.queryVendorProducts = function(vendorId, productId) {
            if (Authentication.isAuthenticated()) {
                var deferred = $q.defer();
                $http({
                    method: vendorProductApi.getVendorProductList.method.query,
                    url: vendorProductApi.getVendorProductList.url + vendorId + '/products',
                    dataType: 'json',
                    contentType: 'application/json'
                }).then(function(response) {
                    function getName(product, callback) {
                        return callback(product.name);
                    }
                    var productNames = [];
                    var prom = [];
                    var singleProduct = null;
                    response.data.results.forEach(function(product) {
                        if (productId && productId.toString() === product.id.toString()) {
                            singleProduct = product;
                        }
                        prom.push(getName(product, function(name) {
                            productNames.push(name.toLowerCase());
                        }));
                    });
                    $q.all(prom).then(function() {
                        var res = {
                            status: 'success',
                            data: (productId) ? singleProduct : response.data.results,
                            productNames: productNames
                        };
                        deferred.resolve(res);
                    });
                }, function(response) {
                    var res = {
                        status: 'error',
                        errors: response.data.reasons
                    };
                    deferred.reject(res);
                });
                return deferred.promise;
            }
        };


        factory.addVendorProduct = function(vendorId, details) {
            var deferred = $q.defer();
            if (Authentication.isAuthenticated()) {
                $http({
                    method: vendorProductApi.addProduct.method,
                    url: vendorProductApi.addProduct.url + vendorId + '/products',
                    dataType: 'json',
                    data: JSON.stringify(details),
                    contentType: 'application/json'
                }).then(function(response) {
                    var res = {
                        status: 'success',
                        data: response.data.results
                    };
                    deferred.resolve(res);
                }, function(response) {
                    var res = {
                        status: 'error',
                        errors: response.data.reasons
                    };
                    deferred.reject(res);
                });
            } else {
                deferred.reject({
                    status: 'error',
                    reasons: 'Do not have permission. If you think this is an error please contact your supervisor.'
                });
            }
            return deferred.promise;
        };

        factory.updateProduct = function(vendorId, details) {
            if (Authentication.isAuthenticated()) {
                var deferred = $q.defer();
                $http({
                    method: vendorProductApi.updateDeleteProduct.method.update,
                    url: vendorProductApi.updateDeleteProduct.url + vendorId + '/products/' + details.id,
                    dataType: 'json',
                    data: JSON.stringify(details),
                    contentType: 'application/json'
                }).then(function(response) {
                    var res = {
                        status: 'success',
                        data: response.data.results,
                    };
                    deferred.resolve(res);
                }, function(response) {
                    var res = {
                        status: 'error',
                        errors: response.data.reasons
                    };
                    deferred.reject(res);
                });
                return deferred.promise;
            }
        };

        factory.deleteProduct = function(vendorId, details) {
            if (Authentication.isAuthenticated()) {
                var deferred = $q.defer();
                $http({
                    method: vendorProductApi.updateDeleteProduct.method.delete,
                    url: vendorProductApi.updateDeleteProduct.url + vendorId + '/products/' + details.id,
                    dataType: 'json',
                    data: JSON.stringify(details),
                    contentType: 'application/json'
                }).then(function(response) {
                    var res = {
                        status: 'success',
                        data: response.data.results,
                    };
                    deferred.resolve(res);
                }, function(response) {
                    var res = {
                        status: 'error',
                        errors: response.data.reasons
                    };
                    deferred.reject(res);
                });
                return deferred.promise;
            }
        };

        // factory.queryVendor = function(vendorId) {
        //     if (Authentication.isAuthenticated()) {
        //         var deferred = $q.defer();
        //         $http({
        //             method: vendorApi.getVendorList.method.query,
        //             url: vendorApi.getVendorList.url + vendorId,
        //             dataType: 'json',
        //             contentType: 'application/json'
        //         }).then(function(response) {
        //             var res = {
        //                 status: 'success',
        //                 data: response.data.results,
        //             };
        //             deferred.resolve(res);
        //         }, function(response) {
        //             var res = {
        //                 status: 'error',
        //                 errors: response.data.reasons
        //             };
        //             deferred.reject(res);
        //         });
        //         return deferred.promise;
        //     }
        // };

        return factory;
    }]);