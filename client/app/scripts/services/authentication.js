'use strict';

/**
 * @ngdoc service
 * @name clientApp.Authentication
 * @description
 * # Authentication
 * Factory in the clientApp.
 */
angular.module('clientApp')
    .factory('Authentication', ['$http', 'jwtHelper', 'localStorageService', '$q',
        function($http, jwtHelper, localStorageService, $q) {
            var baseURI = 'http://middlewareserver.hermannit.com/';
            var loginApi = {
                login: {
                    url: baseURI + 'api-token-auth/',
                    method: 'POST',
                    roles: ['all']
                },
                logout: {
                    url: baseURI + 'api/logout/',
                    method: 'POST',
                    roles: ['all']
                },
                userInfo: {
                    url: baseURI + 'api/user-info/',
                    method: 'POST',
                    roles: ['all']
                }
            };

            var authentication = {};

            authentication.authenticate = function(data, callback) {
                $http({
                    method: loginApi.login.method,
                    skipAuthorization: true,
                    url: loginApi.login.url,
                    data: JSON.stringify(data),
                    dataType: 'json',
                    contentType: 'application/json'
                }).then(function(response) {
                    // set the token
                    authentication.setAuthenticatedToken(response.data.token);
                    var resp = {
                        status: 'success',
                        data: {}
                    };
                    // authentication.setUserInfos();
                    callback(resp);
                }, function(error) {
                    var resp = {
                        status: 'error',
                        data: {}
                    };
                    if (error.data && error.data.non_field_errors) {
                        resp.data = {
                            reasons: error.data.non_field_errors
                        };
                    }
                    callback(resp);
                });
            };

            authentication.isAuthenticated = function() {
                return true;
                // var token = localStorageService.get('token');
                // if (token) {
                //     // console.log(jwtHelper.getTokenExpirationDate(token));
                //     // console.log(jwtHelper.isTokenExpired(token));
                //     // console.log(jwtHelper.decodeToken(token));  
                //     if (jwtHelper.isTokenExpired(token)) {
                //         authentication.clearAuthenticationToken();
                //         return false;
                //     } else {
                //         return true;
                //     }
                // } else {
                //     return false;
                // }
            };

            authentication.setAuthenticatedToken = function(token) {
                localStorageService.set('token', token);
                localStorageService.set('refresh_token', token);
            };

            authentication.clearAuthenticationToken = function() {
                localStorageService.clearAll();
            };

            authentication.getUserInfos = function() {
                var token = localStorageService.get('token');
                var jwt = jwtHelper.decodeToken(token);
                var deferred = $q.defer();
                $http({
                    method: loginApi.userInfo.method,
                    url: loginApi.userInfo.url,
                    data: JSON.stringify({ 'user_id': jwt.user_id }),
                    dataType: 'json',
                    contentType: 'application/json'
                }).then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error);
                });
                return deferred.promise;

            };

            authentication.logout = function() {
                authentication.clearAuthenticationToken();
            };

            authentication.getUserRole = function() {
                return [];
            };

            return authentication;
        }
    ]);