'use strict';

/**
 * @ngdoc service
 * @name clientApp.vendor
 * @description
 * # vendor
 * Factory in the clientApp.
 */
angular.module('clientApp')
    .factory('Vendor', ['$q', '$http', 'Authentication', function($q, $http, Authentication) {
        var factory = {};
        var baseURI = 'http://localhost:3000/';
        var vendorApi = {
            getVendorList: {
                url: baseURI + 'vendors/',
                method: {
                    query: 'GET',
                    update: 'PUT'
                },
                roles: ['all']
            },
            addVendor: {
                url: baseURI + 'vendors/',
                method: 'POST',
                roles: ['all']
            },
            getVendor: {
                url: baseURI + 'vendors/',
                method: 'POST',
                roles: ['all']
            }
        };

        factory.queryVendors = function() {
            if (Authentication.isAuthenticated()) {
                var deferred = $q.defer();
                $http({
                    method: vendorApi.getVendorList.method.query,
                    url: vendorApi.getVendorList.url,
                    dataType: 'json',
                    contentType: 'application/json'
                }).then(function(response) {
                    function getName(vendor, callback) {
                        return callback(vendor.name);
                    }
                    var vendorNames = [];
                    var prom = [];
                    response.data.results.forEach(function(vendor) {
                        prom.push(getName(vendor, function(name) {
                            vendorNames.push(name.toLowerCase());
                        }));
                    });
                    $q.all(prom).then(function() {
                        var res = {
                            status: 'success',
                            data: response.data.results,
                            vendorNames: vendorNames
                        };
                        deferred.resolve(res);
                    });
                }, function(response) {
                    var res = {
                        status: 'error',
                        errors: response.data.reasons
                    };
                    deferred.reject(res);
                });
                return deferred.promise;
            }
        };

        factory.addVendor = function(details) {
            var deferred = $q.defer();
            if (Authentication.isAuthenticated()) {
                $http({
                    method: vendorApi.addVendor.method,
                    url: vendorApi.addVendor.url,
                    dataType: 'json',
                    data: JSON.stringify(details),
                    contentType: 'application/json'
                }).then(function(response) {
                    var res = {
                        status: 'success',
                        data: response.data.results
                    };
                    deferred.resolve(res);
                }, function(response) {
                    var res = {
                        status: 'error',
                        errors: response.data.reasons
                    };
                    deferred.reject(res);
                });
            } else {
                deferred.reject({
                    status: 'error',
                    reasons: 'Do not have permission. If you think this is an error please contact your supervisor.'
                });
            }
            return deferred.promise;
        };

        factory.updateVendor = function(details) {
            if (Authentication.isAuthenticated()) {
                var deferred = $q.defer();
                $http({
                    method: vendorApi.getVendorList.method.update,
                    url: vendorApi.getVendorList.url + details.id,
                    dataType: 'json',
                    data: JSON.stringify(details),
                    contentType: 'application/json'
                }).then(function(response) {
                    var res = {
                        status: 'success',
                        data: response.data.results,
                    };
                    deferred.resolve(res);
                }, function(response) {
                    var res = {
                        status: 'error',
                        errors: response.data.reasons
                    };
                    deferred.reject(res);
                });
                return deferred.promise;
            }
        };

        factory.queryVendor = function(vendorId) {
            if (Authentication.isAuthenticated()) {
                var deferred = $q.defer();
                $http({
                    method: vendorApi.getVendorList.method.query,
                    url: vendorApi.getVendorList.url + vendorId,
                    dataType: 'json',
                    contentType: 'application/json'
                }).then(function(response) {
                    var res = {
                        status: 'success',
                        data: response.data.results,
                    };
                    deferred.resolve(res);
                }, function(response) {
                    var res = {
                        status: 'error',
                        errors: response.data.reasons
                    };
                    deferred.reject(res);
                });
                return deferred.promise;
            }
        };

        factory.queryVendorProducts = function(vendorId) {
            if (Authentication.isAuthenticated()) {
                var deferred = $q.defer();
                $http({
                    method: vendorApi.getVendorList.method.query,
                    url: vendorApi.getVendorList.url + vendorId + '/products',
                    dataType: 'json',
                    contentType: 'application/json'
                }).then(function(response) {
                    function getName(vendor, callback) {
                        return callback(vendor.name);
                    }
                    var productNames = [];
                    var prom = [];
                    response.data.results.forEach(function(product) {
                        prom.push(getName(product, function(name) {
                            productNames.push(name.toLowerCase());
                        }));
                    });
                    $q.all(prom).then(function() {
                        var res = {
                            status: 'success',
                            data: response.data.results,
                            productNames: productNames
                        };
                        deferred.resolve(res);
                    });
                }, function(response) {
                    var res = {
                        status: 'error',
                        errors: response.data.reasons
                    };
                    deferred.reject(res);
                });
                return deferred.promise;
            }
        };

        return factory;
    }]);