'use strict';

/**
 * @ngdoc directive
 * @name clientApp.directive:customTable
 * @description
 * # customTable
 */
angular.module('clientApp')
    .directive('customTable', ['$rootScope', function($rootScope) {
        return {
            templateUrl: 'views/directives/custom.table.html',
            controller: 'CustomTableController',
            controllerAs: 'CustomTableCtrl',
            restrict: 'E',
            scope: {
                headings: '=',
                options: '=',
                callback: '='
            },
            link: function(scope, elm, attrs) {
                scope.displayKeys = scope.headings.slice();
                scope.options = scope.options ? scope.options : [];
                if (scope.options.length < 0) {
                    scope.showFilter = false;
                } else {
                    scope.showFilter = true;
                }
                $rootScope.$on(attrs.datalistready, function(evt, data) {
                    scope.$emit('dataReceived', data);
                });
            }
        };
    }])
    .controller('CustomTableController', ['$scope', function($scope) {
        $scope.showDetails = function(id) {
            $scope.callback(id);
        };

        $scope.$on('dataReceived', function(evt, data) {
            $scope.data = data;
            $scope.originalList = data;
            $scope.totalitems = data.length;
            $scope.$watch('filterOptions', function() {
                $scope.data = $scope.filterData();
                $scope.totalitems = data.length;
            }, true);
        });

        $scope.config = {
            itemsPerPage: 5,
            fillLastPage: true
        };

        $scope.filterOptions = {
            filterBy: $scope.options.length > 0 ? $scope.options[0] : '',
            value: ''
        };

        $scope.filterData = function() {
            if ($scope.originalList) {
                return $scope.originalList.filter(function(value) {
                    if ($scope.filterOptions.value.trim() !== '') {
                        if (value[$scope.filterOptions.filterBy]) {
                            var myValue = value[$scope.filterOptions.filterBy];
                            if (typeof value[$scope.filterOptions.filterBy] !== 'string') {
                                myValue = myValue.toString();
                            }
                            return myValue.toLowerCase().includes($scope.filterOptions.value.trim().toLowerCase());
                        }
                        return false;
                    }
                    return true;
                });
            }

        };



        $scope.goToPage = function(newpage, oldpage) {
            if (newpage !== oldpage) {
                var startIndex = (newpage - 1) * $scope.config.itemsPerPage;
                var data = $scope.filterData();
                $scope.data = data.slice(startIndex);
            }
        };
    }]);