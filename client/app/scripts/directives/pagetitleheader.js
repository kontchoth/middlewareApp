'use strict';

/**
 * @ngdoc directive
 * @name clientApp.directive:pageTitleHeader
 * @description
 * # pageTitleHeader
 */
angular.module('clientApp')
    .directive('pageTitleHeader', function() {
        return {
            templateUrl: 'views/directives/page.title.template.html',
            controller: 'PageTemplateHeaderController',
            controllerAs: 'titleCtrl',
            restrict: 'E',
            scope: {
                user: '=',
                msg: '=',
                addlabel: '@addlabel',
                url: '@url'
            }

        };
    })
    .controller('PageTemplateHeaderController', ['$scope',
        function($scope) {
            $scope.init = function() {
                // Authentication.getUserInfos().then(function(res){
                //     $scope.user = res;
                // }, function(error) {
                //     console.log('pahe title header', res);
                // });
            };
        }
    ]);