'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:VendorsCtrl
 * @description
 * # VendorsCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
    .controller('VendorsCtrl', ['$location', 'Authentication', 'Vendor', 'VendorProduct', '$routeParams', '$scope', '$rootScope',
        function($location, Authentication, Vendor, VendorProduct, $routeParams, $scope, $rootScope) {
            $scope.new_vendor = false;
            $scope.vendorProducts = [];
            $scope.errors = [];
            $scope.messages = [];
            $scope.ready = false;
            $scope.notificationTime = 5000;
            $scope.init = function() {
                if (Authentication.isAuthenticated()) {
                    Authentication.getUserInfos().then(function(res) {
                        $scope.user = res;
                    }, function(error) {
                        console.log('pahe title header', error);
                    });
                }
                $scope.message = 'Please select a vendor you would like to review.';
                $scope.getVendorList();
                $scope.headings = [{
                    title: 'Name',
                    flex: '20',
                    name: 'name'
                }, {
                    title: 'Street Name',
                    flex: '20',
                    name: 'streetname'
                }, {
                    title: 'City',
                    flex: '20',
                    name: 'city'
                }, {
                    title: 'State',
                    flex: '20',
                    name: 'state'
                }, {
                    title: 'Zip Code',
                    flex: '20',
                    name: 'zipcode'
                }];

                $scope.productHeadings = [{
                    title: 'Name',
                    flex: '25',
                    name: 'name'
                }, {
                    title: 'Unit',
                    flex: '25',
                    name: 'unit'
                }, {
                    title: 'Unit QTY',
                    flex: '25',
                    name: 'unit_qty'
                }, {
                    title: 'Unit Price',
                    flex: '25',
                    name: 'unit_price'
                }];
                if ($routeParams.vendor_id) {
                    if ($routeParams.vendor_id === 'new') {
                        $scope.new_vendor = true;
                        $scope.vendor = $scope.getVendorInfos();
                    } else {
                        Vendor.queryVendor($routeParams.vendor_id).then(function(response) {
                            $scope.vendor = response.data;
                            VendorProduct.queryVendorProducts($routeParams.vendor_id).then(function(response) {
                                $scope.vendorProducts = response.data;
                                $scope.productNames = response.productNames;
                                $rootScope.$emit('vendorProductsReady', response.data);
                            }, function(error) {
                                console.log(error);
                            });
                        }, function(error) {
                            console.log(error);
                        });
                    }
                }

                $scope.addressTypes = ['Business', 'Residential'];

                $scope.states = ('AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS ' +
                    'MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI ' +
                    'WY').split(' ').map(function(state) {
                    return { abbrev: state };
                });
            };

            $scope.removeNotification = function(msg, type) {
                var index = -1;
                switch (type) {
                    case 'error':
                        index = $scope.errors.indexOf(msg);
                        if (index > -1) {
                            $scope.errors.splice(index, 1);
                        }
                        break;
                    case 'message':
                        index = $scope.messages.indexOf(msg);
                        if (index > -1) {
                            $scope.messages.splice(index, 1);
                        }
                        break;
                    default:
                        break;
                }
            };

            $scope.vendorDetail = function(id) {
                $location.path('vendors/' + id);
            };

            $scope.getVendorInfos = function(vendor_id) {
                if (vendor_id) {
                    return {};
                }
                return {
                    name: '',
                    address: {
                        address_type: 'Bussiness',
                        streetname: '',
                        city: '',
                        state: 'MD',
                        zipcode: ''
                    },
                    contact: {
                        firstname: '',
                        middlename: '',
                        lastname: '',
                        position: '',
                        email: '',
                        phone: '',
                        fax: ''
                    }
                };
            };

            $scope.getVendorList = function() {
                Vendor.queryVendors().then(function(response) {
                    if (response.status === 'success') {
                        $scope.vendors = response.data;
                        $scope.vendorNames = response.vendorNames;
                        $rootScope.$emit('vendorListReady', response.data);
                        $scope.ready = true;
                    } else {
                        $scope.vendors = [];
                        $scope.vendorNames = [];
                    }
                }, function(error) {
                    console.log(error);
                });
            };

            $scope.saveOrUpdate = function() {
                if ($scope.new_vendor) {
                    // check to see if there is a duplicate
                    var duplicate = $scope.vendorNames.indexOf($scope.vendor.name.toLowerCase()) > -1;
                    if (duplicate) {
                        var index = $scope.errors.indexOf(CUSTOM_MSG.duplicate);
                        if (index === -1) {
                            $scope.errors.push(CUSTOM_MSG.duplicate);
                            setTimeout(function() {
                                removeNotification(CUSTOM_MSG.duplicate, 'error');
                            }, $scope.notificationTime);
                        }

                    } else {
                        Vendor.addVendor($scope.vendor).then(function(response) {
                            if ($scope.debug) {
                                console.log(response);
                            }
                            $location.path('vendors');
                        }, function(error) {
                            console.log(error);
                        });
                    }
                } else {
                    // update the information
                    Vendor.updateVendor($scope.vendor).then(function(response) {
                        if ($scope.debug) {
                            console.log(response);
                        }

                        $scope.messages.push(CUSTOM_MSG.updateSuccess);
                        setTimeout(function() {
                            removeNotification(CUSTOM_MSG.updateSuccess, 'message');
                        }, $scope.notificationTime);
                    }, function(error) {
                        if ($scope.debug) {
                            console.log(error);
                        }
                        $scope.errors.push(CUSTOM_MSG.updateError);
                        setTimeout(function() {
                            removeNotification(CUSTOM_MSG.updateError, 'error');
                        }, $scope.notificationTime);
                    });
                }
            };

            function removeNotification(msg, msg_type) {
                var index = (msg_type === 'error') ? $scope.errors.indexOf(msg) : $scope.messages.indexOf(msg);
                console.log('removing message at index', index);
                if (index > -1) {
                    switch (msg_type) {
                        case 'error':
                            $scope.errors.splice(index, 1);
                            $scope.$apply();
                            break;
                        case 'message':
                            $scope.messages.splice(index, 1);
                            $scope.$apply();
                            break;
                        default:
                            break;
                    }
                }
            }

            $scope.productVendorDetail = function(productID) {
                $location.path('vendors/' + $routeParams.vendor_id + '/' + productID);
            };

            $scope.addNewProduct = function() {
                $location.path('vendors/' + $routeParams.vendor_id + '/' + 'new');
            };

            var CUSTOM_MSG = {
                duplicate: 'There is already a vendor with that name in the system.',
                updateSuccess: 'Vendor information has been updated.',
                updateError: 'There was an error updating the vendor information. Please try again later.'
            };
        }
    ]);