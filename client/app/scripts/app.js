'use strict';

/**
 * @ngdoc overview
 * @name clientApp
 * @description
 * # clientApp
 *
 * Main module of the application.
 */
angular
    .module('clientApp', [
        'ngAnimate',
        'ngAria',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        // 'ngTouch',
        'restangular',
        'angular-jwt',
        'LocalStorageModule',
        'angularUtils.directives.dirPagination',
        'ngMaterial',
        'ngMdIcons',
    ])
    .config(function(localStorageServiceProvider) {
        localStorageServiceProvider
            .setPrefix('middleware-ui')
            .setStorageType('localStorage')
            .setDefaultToCookie(false)
            .setNotify(true, true);
    })
    .config(['$qProvider', function($qProvider) {
        $qProvider.errorOnUnhandledRejections(false);
    }])
    .config(['$locationProvider', function($locationProvider) {
        $locationProvider.html5Mode(true);
    }])
    .config(function($httpProvider, jwtOptionsProvider) {
        jwtOptionsProvider.config({
            tokenGetter: ['options', 'localStorageService', function(options, localStorageService) {
                if (options && options.url.substr(options.url.length - 5) === '.html') {
                    return null;
                }

                if (options) {
                    //console.log('Cannot skipped', options.url);
                }
                var token = localStorageService.get('token');
                return token;
            }],
            whiteListedDomains: ['localhost', 'middleware.hermannit.com', 'middlewareserver.hermannit.com']
        });

        $httpProvider.interceptors.push('jwtInterceptor');
    })
    // .run(function(authManager) {
    //     authManager.checkAuthOnRefresh();
    // })
    .run(function($rootScope, $location, Authentication) {

        // enumerate routes that don't need authentication
        var routesThatDontRequireAuth = ['/login'];

        // check if current location matches route  
        var routeClean = function(route) {
            return routesThatDontRequireAuth.find(
                function(noAuthRoute) {
                    return route.startsWith(noAuthRoute);
                }
            );
        };
        $rootScope.$on('$routeChangeStart', function(event, next, current) {
            // if route requires auth and user is not logged in
            if (!routeClean($location.url()) && !Authentication.isAuthenticated()) {
                // redirect back to login
                $location.path('/login');
            }
        });
    })
    .config(function($routeProvider, RestangularProvider) {
        RestangularProvider.setBaseUrl('http://localhost:3000');
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                controllerAs: 'main'
            })
            .when('/about', {
                templateUrl: 'views/about.html',
                controller: 'AboutCtrl',
                controllerAs: 'about'
            })
            .when('/vendors', {
                templateUrl: 'views/vendors.html',
                controller: 'VendorsCtrl',
                controllerAs: 'vendors'
            })
            .otherwise({
                redirectTo: '/'
            });
    })
    .factory('VendorRestangular', function(Restangular) {
        return Restangular.withConfig(function(RestangularConfigurer) {
            RestangularConfigurer.setRestangularFields({
                id: '_id'
            });
        });
    }).factory('Vendor', function(VendorRestangular) {
        return VendorRestangular.service('vendors');
    });